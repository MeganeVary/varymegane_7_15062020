class RecipeList {
  constructor() {
    this.recipeArray = [];
    this.uniqueIngredients = [];
    this.uniqueAppliance = [];
    this.uniqueUstensils = [];
    this.arrayTag = [];
    this.dictionaire = [];
    // this.arrayButtonTag = [];
  }

  /**
   * Return true if dictionnary item already contains
   * given recipe.
   *
   * @param {int} recipeId
   * @param {array} dictionnaryItem
   */
  arrayContainsRecipe(recipeId, dictionnaryItem) {
    let alreadyExists = false;
  console.log(dictionnaryItem)
    if(dictionnaryItem === undefined){
      return false
    }
    for (let recipe of dictionnaryItem) {

      if (recipe.id === recipeId) {
        alreadyExists = true;
        break;
      }
    }

    return alreadyExists;
  }

  SetDictionaire() {
    let regexSplit = /[ ,]+/; // manque les points\

    for (let recipe of this.recipeArray) {
      let applianceArray = recipe.appliance.toLowerCase().split(regexSplit);
      let nameArray = recipe.name.toLowerCase().split(regexSplit);
      let ustensilsArray = recipe.ustensils.toString().toLowerCase().split(regexSplit); // to lowercase
      let ingredientsArray = recipe.ingredients;

      for (let ingredient of ingredientsArray) {
        ingredientsArray = ingredient.name.toLowerCase().split(regexSplit);

        for (let ingredientName of ingredientsArray) {
          if (ingredientName in this.dictionaire && !this.arrayContainsRecipe(recipe.id, this.dictionaire[ingredientName])) {
            this.dictionaire[ingredientName].push(recipe);
          }

          else if (!(ingredientName in this.dictionaire)) {
            this.dictionaire[ingredientName] = [recipe];
          }
        }
      }
      for (let Appliance of applianceArray) {
        applianceArray = Appliance.toLowerCase().split(regexSplit);

        for (let ApplianceName of applianceArray) {
          if (ApplianceName in this.dictionaire && !this.arrayContainsRecipe(recipe.id, this.dictionaire[ApplianceName])) {
            this.dictionaire[ApplianceName].push(recipe);
          }

          else if (!(ApplianceName in this.dictionaire)) {
            this.dictionaire[ApplianceName] = [recipe];
          }
        }
      }
      for (let Ustensil of ustensilsArray) {
        ustensilsArray = Ustensil.toLowerCase().split(regexSplit);

        for (let UstensilName of ustensilsArray) {
          if (UstensilName in this.dictionaire && !this.arrayContainsRecipe(recipe.id, this.dictionaire[UstensilName])) {
            this.dictionaire[UstensilName].push(recipe);
          }

          else if (!(UstensilName in this.dictionaire)) {
            this.dictionaire[UstensilName] = [recipe];
          }
        }
      }
      for (let recipeName of nameArray) {
        nameArray = recipeName.toLowerCase().split(regexSplit);

        for (let recipetitle of nameArray) {
          if (recipetitle in this.dictionaire && !this.arrayContainsRecipe(recipe.id, this.dictionaire[recipetitle])) {
            this.dictionaire[recipetitle].push(recipe);
          }

          else if (!(recipetitle in this.dictionaire)) {
            this.dictionaire[recipetitle] = [recipe];
          }
        }
      }
    }
  }

  tagDeleted(dataset, dataType) {
    this.arrayTag = this.arrayTag.filter((tag) => {
      return tag.tagName !== dataset;
    });
    this.filterRecipe();
  }

  /**
   * Inject filtered recipes in DOM from this.tagsArray
   *
   * @param {string} dataset
   * @param {string} dataType
   */
  filterRecipe(dataset = null, dataType = null) {
    let inputPrincipal = document.getElementById("searchInputPrincipal");
    var containerCard = document.querySelector(".containerCard");
    containerCard.innerHTML = "";
    if (dataset && dataType && this.countTags(dataset, dataType, 0) === 0) {
      let dataTag = TagArray.getArrayTag(dataset, dataType);
      this.arrayTag.push(dataTag);
      this.getButtonTag(dataset, dataType);
    }
    let recipes = this.recipeArray;
    for (let filter of this.arrayTag) {
      if (filter.tagType === "ingredient") {
        recipes = this.getcardByIngredient(recipes, filter.tagName);
      } else if (filter.tagType === "ustensile") {
        recipes = this.getcardByUstensils(recipes, filter.tagName);
      } else if (filter.tagType === "appareil") {
        recipes = this.getcardByAppliance(recipes, filter.tagName);
      } else if (filter.tagType === "searchPrincipal") {
        recipes = this.getcardByAll(recipes, filter.tagName, this.dictionaire);
      }
    }
    if (recipes && recipes.length > 0) {
      document.getElementById("noResult").style.display = "none";
      for (let recipePrint of recipes) {
        containerCard.innerHTML += HTMLConstructor.getRecipeCard(recipePrint);
      }
    } else {
      document.getElementById("noResult").style.display = "flex";
    }
    inputPrincipal.value = '';
  }

  getcardByAll(recipes = [], tagData, dictionaire = []) {
    let regexSplit = /[ ,]+/;
    let newrecipesArray = [];
    let newTagData = tagData.split(regexSplit);
    for (let tag of newTagData) {
      if (newrecipesArray.length === 0) {
        if(tag in dictionaire){
          for (let recipe of dictionaire[tag]) {
            newrecipesArray.push(recipe)
          }
        }
      } else {
        for (let recipe of newrecipesArray) {
          if (!(this.arrayContainsRecipe(recipe.id, dictionaire[tag]))) {
            newrecipesArray = newrecipesArray.filter(function (newRecipe) {
              return newRecipe.name != recipe.name;
            });
          }
        }
      }
    }
    return newrecipesArray;
  }
  getcardByUstensils(recipes = [], ustensilData) {
    let newrecipesArray = [];
    for (let recipe of recipes) {
      for (let ustensil of recipe.ustensils) {
        if (ustensil.toLowerCase().includes(ustensilData.toLowerCase())) {
          newrecipesArray.push(recipe);
        }
      }
    }
    return newrecipesArray;
  }

  getcardByIngredient(recipes = [], ingredientData) {
    let newrecipesArray = [];
    for (let recipe of recipes) {
      for (let ingredient of recipe.ingredients) {
        if (
          ingredient.name.toLowerCase().includes(ingredientData.toLowerCase())
        ) {
          newrecipesArray.push(recipe);
        }
      }
    }
    console.log(newrecipesArray);

    return newrecipesArray;
  }
  getcardByAppliance(recipes = [], applianceData) {
    let newrecipesArray = [];
    for (let recipe of recipes) {
      if (
        recipe.appliance.toLowerCase().includes(applianceData.toLowerCase())
      ) {
        newrecipesArray.push(recipe);
      }
    }
    return newrecipesArray;
  }

  getButtonTag(tagValue, dataType) {
    var containerButtonAdd = document.querySelector("#elements_add");
    if (tagValue) {
      containerButtonAdd.innerHTML += `
        <button onclick="window.cancelTag(event)" data-type="${dataType}" data-value="${tagValue}" type="button" class="button_${dataType}_selectionne btn mr-1">
          <i class="far fa-times-circle mr-1"></i>${tagValue}
        </button>`;
    }
  }

  countTags(tagValue, dataType, maxCount = 0) {
    let count = 0;
    for (let tag of this.arrayTag) {
      if (tag.tagName == tagValue && tag.tagType == dataType) {
        count++;
      }
    }

    return count;
  }

  pushRecipe(recipe) {
    this.recipeArray.push(recipe);
    this.getAddIngredientUnique(recipe.ingredients);
    this.getAddApplianceUnique(recipe.appliance.split());
    this.getAddUstensilsUnique(recipe.ustensils);
  }

  getAddIngredientUnique(ingredients = []) {
    for (let ingredient of ingredients) {
      if (!this.uniqueIngredients.includes(ingredient.name)) {
        this.uniqueIngredients.push(ingredient.name);
      }
    }
  }
  getAddApplianceUnique(applianceArray = []) {
    for (let appliance of applianceArray) {
      if (!this.uniqueAppliance.includes(appliance)) {
        this.uniqueAppliance.push(appliance);
      }
    }
  }
  getAddUstensilsUnique(ustensilsArray = []) {
    for (let ustensil of ustensilsArray) {
      if (!this.uniqueUstensils.includes(ustensil)) {
        this.uniqueUstensils.push(ustensil);
      }
    }
  }

  // suggestion chevron
  static getIngredientsSuggestion(ingredients) {
    return HTMLConstructor.getItemSuggestionHTML(ingredients);
  }
  static getAppliancesSuggestion(applianceArray) {
    return HTMLConstructor.getItemSuggestionHTML(applianceArray);
  }
  static getUstensilsSuggestion(ustensilsArray) {
    return HTMLConstructor.getItemSuggestionHTML(ustensilsArray);
  }

  // suggestion by input search
  static getSearchIngredient(ingredients) {
    const n = 5; //get the first 5 item
    var containerInput = document.getElementById(
      "containerInputSearchIngredients"
    );
    // var buttonIngredient = document.getElementById("button_ingredient");
    // var buttonSearch = document.getElementById("button_search_ingredient");
    const searchInputPrincipal = document.getElementById(
      "button_search_ingredient"
    );
    var valueSearch = searchInputPrincipal.value;
    let ingredientsArray = [];

    let inputs = document.querySelectorAll(".btn_filtre");
    for (let input of inputs) {
      input.addEventListener("input", function (e) {
        if (e.currentTarget.value !== "") {
          e.currentTarget
            .closest(".dropdown-container")
            .classList.remove("dropdown-open");
          e.currentTarget
            .closest(".dropdown-container")
            .classList.remove("col-6");
        }
      });
    }

    for (let ingredient of ingredients) {
      if (ingredient.toLowerCase().includes(valueSearch.toLowerCase())) {
        if (!ingredientsArray.includes(ingredient.toLowerCase())) {
          ingredientsArray.push(ingredient.toLowerCase());
        }
      }
      if (ingredientsArray.length >= n) {
        break;
      }
    }
    HTMLConstructor.getSearchIngredientsSuggestionHTML(ingredientsArray);
  }

  static getSearchAppliance(applianceArray) {
    const n = 5; //get the first 5 item
    const searchInputPrincipal = document.getElementById(
      "button_search_appliance"
    );
    var valueSearch = searchInputPrincipal.value;
    let applianceArraybis = [];
    for (let appliance of applianceArray) {
      if (appliance.toLowerCase().includes(valueSearch.toLowerCase())) {
        if (!applianceArraybis.includes(appliance.toLowerCase())) {
          applianceArraybis.push(appliance.toLowerCase());
        }
      }
      if (applianceArraybis.length >= n) {
        break;
      }
    }
    HTMLConstructor.getSearchApplianceSuggestionHTML(applianceArraybis);
  }

  static getSearchUstensil(ustensilsArray) {
    const n = 5; //get the first 5 item
    const searchInputPrincipal = document.getElementById(
      "button_search_ustensil"
    );
    var valueSearch = searchInputPrincipal.value;
    let ustensilArraybis = [];
    for (let ustensil of ustensilsArray) {
      if (ustensil.toLowerCase().includes(valueSearch.toLowerCase())) {
        if (!ustensilArraybis.includes(ustensil.toLowerCase())) {
          ustensilArraybis.push(ustensil.toLowerCase());
        }
      }
      if (ustensilArraybis.length >= n) {
        break;
      }
    }
    HTMLConstructor.getSearchUstensilSuggestionHTML(ustensilArraybis);
  }
}
