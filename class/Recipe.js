
class Recipe {
    constructor(appliance,description,id,ingredients,name,servings,time,ustensils){
        this.that = this;
        this.appliance = appliance;
        this.description = description;
        this.id = id;
        this.ingredients = [];
        this.name = name;
        this.servings = servings;
        this.time = time;
        this.ustensils = ustensils;
        this.setIngredients(ingredients);
    }
    setIngredients(ingredients) {
        for (let ingredient of ingredients) {
            this.ingredients.push(
                new Ingredient(ingredient.ingredient, ingredient.quantity, ingredient.unit)
            );
        }
    }
    // get Card recipe
    getCard() {
        return HTMLConstructor.getRecipeCard(this);
    }
    getIngredientsHTML(){
        return HTMLConstructor.getIngredientsHTML(this);
    }
}
