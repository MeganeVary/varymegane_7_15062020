class Ingredient {

 constructor(name, quantity, unit) 
  {
   this.name = name;
   this.unit = unit;
   this.quantity = quantity;
  }
}