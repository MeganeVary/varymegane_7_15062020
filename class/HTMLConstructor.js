class HTMLConstructor {
  static getRecipeCard(recipe) {
    return `
          <article class="col-lg-4 col-sm-12 m-10" style="margin: 20px 0px 20px 0px;">
              <div class="image_article"></div>
              <section class="information_article d-flex flex-wrap flex-row">
                  <h3 class="col-9">${recipe.name}</h3>
                  <div class="timer col-3">
                      <span><i class="far fa-clock"></i> ${
                        recipe.time
                      } min</span>
                  </div>
                  <div class="liste_ingredient col-6">
                    ${this.getIngredientsHTML(recipe)}
                  </div>
                  <div class="liste_instruction col-6 ">
                  <p class="text_instruction">${recipe.description}</p>
                  </div>
              </section>
          </article>
          `;
  }


  // ingredients in Card recipe
  static getIngredientsHTML(recipe) {
    let ingredientHtml = "";
    for (let ingredient of recipe.ingredients) {
      ingredientHtml += `
      <p class="">${ingredient.name} ${
        ingredient.quantity || ingredient.unit ? ":" : ""
      }
        <span>${ingredient.quantity ?? ""} ${ingredient.unit ?? ""}</span>
      </p>`;
    }
    return ingredientHtml;
  }

  // get item search by suggestions
  static getItemSuggestionHTML(itemArray) {
    let itemSuggestionHtml = "";
    itemArray.forEach((item) => {
      itemSuggestionHtml += `<a class="lienListItem" id="${item}" data-value="${item}"> ${item}</a><br>`;
    });
    
    return itemSuggestionHtml;
  }

  // get suggestion by input search
  static getSearchIngredientsSuggestionHTML(ingredients) {
    const searchInputIngredient = document.getElementById(
      "button_search_ingredient"
    );
    var valueSearch = searchInputIngredient.value;
    var containerResultSearch = document.getElementById(
      "resultSearchIngredient"
    );
    let suggestionsHTML = "";
    if (valueSearch === "" || valueSearch === " ") {
      containerResultSearch.style.display = "none";
    } else {
      for (let ingredient of ingredients) {
        containerResultSearch.style.display = "block";
        suggestionsHTML += `<li> <a class="lienListItem" onclick="window.searchRecipesBySuggestionClick(event)" id="${ingredient}"  data-value="${ingredient}"> ${ingredient} </a> </li>`;
      }
    }
    return (containerResultSearch.innerHTML = suggestionsHTML);
  }

  static getSearchApplianceSuggestionHTML(appliances) {
    const searchInputAppliance = document.getElementById(
      "button_search_appliance"
    );
    var valueSearch = searchInputAppliance.value;
    var containerResultSearch = document.getElementById(
      "resultSearchAppliance"
    );
    let suggestionsHTML = "";
    if (valueSearch === "" || valueSearch === " ") {
      containerResultSearch.style.display = "none";
    } else {
      for (let appliance of appliances) {
        containerResultSearch.style.display = "block";
        suggestionsHTML +=  `<li> <a class="lienListItem" onclick="window.searchRecipesBySuggestionClick(event)" id="${appliance}"  data-value="${appliance}"> ${appliance} </a> </li>`;
      }
    }
    return (containerResultSearch.innerHTML = suggestionsHTML);
  }

  static getSearchUstensilSuggestionHTML(ustensils) {
    const searchInputAppliance = document.getElementById(
      "button_search_ustensil"
    );
    var valueSearch = searchInputAppliance.value;
    var containerResultSearch = document.getElementById("resultSearchUstensil");
    let suggestionsHTML = "";
    if (valueSearch === "" || valueSearch === " ") {
      containerResultSearch.style.display = "none";
    } else {
      for (let ustensil of ustensils) {
        containerResultSearch.style.display = "block";
        suggestionsHTML += `<li> <a class="lienListItem" onclick="window.searchRecipesBySuggestionClick(event)" id="${ustensil}"  data-value="${ustensil}"> ${ustensil} </a> </li>`
      }
    }
    return (containerResultSearch.innerHTML = suggestionsHTML);
  }
}