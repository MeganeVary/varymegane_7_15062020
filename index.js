import recipes from "./recipes.js";

let recipesData = recipes;
let containerCard = document.querySelector(".containerCard");

// menu dropdown
var containerIngredient = document.querySelectorAll(".liste_ingredient"); // ingredients in card recipe

var containerSugestionIngredients = document.querySelector(
  "#suggestionItemsIngredient"
);
var containerSugestionUstensils = document.querySelector(
  "#suggestionItemsUstensiles"
);
var containerSugestionAppliance = document.querySelector(
  "#suggestionItemsAppareil"
);

const searchInputIngredient = document.getElementById(
  "button_search_ingredient"
);
const searchInputAppliance = document.getElementById("button_search_appliance");
const searchInputUstensils = document.getElementById("button_search_ustensil");

let recipeArray = new RecipeList();

recipesData.forEach((data) => {
  var recipe = new Recipe(
    data.appliance,
    data.description,
    data.id,
    data.ingredients,
    data.name,
    data.servings,
    data.time,
    data.ustensils
  );
  recipeArray.pushRecipe(recipe);
  containerCard.innerHTML += recipe.getCard();
  containerIngredient.innerHTML += recipe.getIngredientsHTML();
});

recipeArray.SetDictionaire();

// search by input search
searchInputIngredient.addEventListener("keyup", function (e) {
  RecipeList.getSearchIngredient(recipeArray.uniqueIngredients);
  const valueInputIngredient = searchInputIngredient.value;
  var dataType = e.currentTarget.closest("input").dataset.type;

  if ((valueInputIngredient.trim() != "") & (e.key == "Enter")) {
    recipeArray.filterRecipe(valueInputIngredient.toLowerCase(), dataType);
    // recipeArray.getButtonTag(valueInputIngredient, dataType);
  }
});
searchInputAppliance.addEventListener("keyup", function (e) {
  RecipeList.getSearchAppliance(recipeArray.uniqueAppliance);
  const valueInputAppliance = searchInputAppliance.value;
  var dataType = e.currentTarget.closest("input").dataset.type;
  if ((valueInputAppliance.trim() != "") & (e.key == "Enter")) {
    recipeArray.filterRecipe(valueInputAppliance.toLowerCase(), dataType);
    // recipeArray.getButtonTag(valueInputAppliance, dataType);
  }
});
searchInputUstensils.addEventListener("keyup", function (e) {
  RecipeList.getSearchUstensil(recipeArray.uniqueUstensils);
  const valueInputUstensil = searchInputUstensils.value;
  var dataType = e.currentTarget.closest("input").dataset.type;
  if ((valueInputUstensil.trim() != "") & (e.key == "Enter")) {
    recipeArray.filterRecipe(valueInputUstensil.toLowerCase(), dataType);
    // recipeArray.getButtonTag(valueInputUstensil, dataType);
  }
});

document.getElementById("searchInputPrincipal").addEventListener("keyup", function (e) {
    const valueInputPrincipal = document.getElementById("searchInputPrincipal").value;
    var dataType = e.currentTarget.closest("input").dataset.type;
    if ((valueInputPrincipal.trim() != "") & (e.key == "Enter")) {
      recipeArray.filterRecipe(valueInputPrincipal.toLowerCase(), dataType);
      // recipeArray.getButtonTag(valueInputPrincipal.toLowerCase(), dataType);
    }
  });

// search by list suggestions
containerSugestionUstensils.innerHTML += RecipeList.getUstensilsSuggestion(
  recipeArray.uniqueUstensils
);
containerSugestionAppliance.innerHTML += RecipeList.getAppliancesSuggestion(
  recipeArray.uniqueAppliance
);
containerSugestionIngredients.innerHTML += RecipeList.getUstensilsSuggestion(
  recipeArray.uniqueIngredients
);

let dropdownArrows = document.querySelectorAll(".fa-chevron-down");

for (let arrow of dropdownArrows) {
  arrow.addEventListener("click", function (e) {
    let dropdownContainer = e.currentTarget.closest(".dropdown-container");
    dropdownContainer.classList.toggle("col-6");
    dropdownContainer.classList.toggle("dropdown-open");
  });
}
var lienListItem = document.querySelectorAll(".lienListItem");

for (let i = 0; i < lienListItem.length; i++) {
  lienListItem[i].addEventListener("click", (e) => {
    searchRecipesBySuggestionClick(e);
  });
}


var buttonTagSelected = document.querySelectorAll(".button_ingredient_selectionne");
for (let i = 0; i < buttonTagSelected.length; i++) {
  buttonTagSelected[i].addEventListener("click", (e) => {
    cancelTag(e);
  });
}

window.searchRecipesBySuggestionClick = (e) => {
  e.preventDefault();
  let arrayTag = [];
  var tagValue = e.currentTarget.dataset.value;
  var dataType = e.currentTarget.closest(".dropdown-button").dataset.type;

  if (dataType == "ingredient") {
    // recipeArray.getButtonTag(tagValue.toLowerCase(), dataType);
    let dropdownContainer = e.currentTarget.closest(".dropdown-container");
    dropdownContainer.classList.toggle("col-6");
    dropdownContainer.classList.toggle("dropdown-open");
  } else if (dataType == "appareil") {
    // recipeArray.getButtonTag(tagValue.toLowerCase(), dataType);
    let dropdownContainer = e.currentTarget.closest(".dropdown-container");
    dropdownContainer.classList.toggle("col-6");
    dropdownContainer.classList.toggle("dropdown-open");
  } else if (dataType == "ustensile") {
    // recipeArray.getButtonTag(tagValue.toLowerCase(), dataType);
    let dropdownContainer = e.currentTarget.closest(".dropdown-container");
    dropdownContainer.classList.toggle("col-6");
    dropdownContainer.classList.toggle("dropdown-open");
  }
  arrayTag.push(tagValue);
  recipeArray.filterRecipe(tagValue.toLowerCase(), dataType);
};

window.cancelTag = (e) => {
  let tagValue = e.currentTarget.dataset.value;

  recipeArray.tagDeleted(tagValue);

  e.currentTarget.remove();
};